package org.ysling.litemall.wx.model.reward.body;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.ysling.litemall.db.entity.PageBody;

/**
 * @author Ysling
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RewardListBody extends PageBody {

}
