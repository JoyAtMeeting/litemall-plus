package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallLog;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 操作日志表 服务类
 * </p>
 *
 * @author ysling
 */
public interface ILogService extends IBaseService<LitemallLog> {

}
