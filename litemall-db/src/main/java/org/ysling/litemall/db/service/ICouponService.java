package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallCoupon;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 优惠券信息及规则表 服务类
 * </p>
 *
 * @author ysling
 */
public interface ICouponService extends IBaseService<LitemallCoupon> {

}
