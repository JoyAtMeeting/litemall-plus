package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallCategory;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 类目表 服务类
 * </p>
 *
 * @author ysling
 */
public interface ICategoryService extends IBaseService<LitemallCategory> {

}
