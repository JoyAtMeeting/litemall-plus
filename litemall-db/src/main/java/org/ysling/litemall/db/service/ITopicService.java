package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallTopic;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 专题表 服务类
 * </p>
 *
 * @author ysling
 */
public interface ITopicService extends IBaseService<LitemallTopic> {

}
