package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallGoodsProduct;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 商品货品表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IGoodsProductService extends IBaseService<LitemallGoodsProduct> {

}
