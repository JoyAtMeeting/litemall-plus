package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallDynamic;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 动态表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IDynamicService extends IBaseService<LitemallDynamic> {

}
