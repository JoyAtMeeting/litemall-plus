package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallRewardTask;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 赏金任务规则表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IRewardTaskService extends IBaseService<LitemallRewardTask> {

}
