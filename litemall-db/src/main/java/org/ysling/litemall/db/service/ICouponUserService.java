package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallCouponUser;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 优惠券用户使用表 服务类
 * </p>
 *
 * @author ysling
 */
public interface ICouponUserService extends IBaseService<LitemallCouponUser> {

}
