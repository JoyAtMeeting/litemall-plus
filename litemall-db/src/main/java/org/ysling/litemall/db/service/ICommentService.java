package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallComment;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 评论表 服务类
 * </p>
 *
 * @author ysling
 */
public interface ICommentService extends IBaseService<LitemallComment> {

}
