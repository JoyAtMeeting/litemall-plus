package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallDealingSlip;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 用户交易记录 服务类
 * </p>
 *
 * @author ysling
 */
public interface IDealingSlipService extends IBaseService<LitemallDealingSlip> {

}
