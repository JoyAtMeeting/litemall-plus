package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallNotice;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 通知表 服务类
 * </p>
 *
 * @author ysling
 */
public interface INoticeService extends IBaseService<LitemallNotice> {

}
