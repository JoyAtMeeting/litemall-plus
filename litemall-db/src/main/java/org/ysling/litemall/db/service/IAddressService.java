package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallAddress;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 收货地址表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IAddressService extends IBaseService<LitemallAddress> {

}
