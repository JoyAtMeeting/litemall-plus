package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallGrouponRules;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 团购规则表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IGrouponRulesService extends IBaseService<LitemallGrouponRules> {

}
