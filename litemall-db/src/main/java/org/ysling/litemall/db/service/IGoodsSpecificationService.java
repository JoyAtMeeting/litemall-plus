package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallGoodsSpecification;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 商品规格表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IGoodsSpecificationService extends IBaseService<LitemallGoodsSpecification> {

}
