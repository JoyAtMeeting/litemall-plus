package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallShare;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 用户分享记录表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IShareService extends IBaseService<LitemallShare> {

}
