package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallNoticeAdmin;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 通知管理员表 服务类
 * </p>
 *
 * @author ysling
 */
public interface INoticeAdminService extends IBaseService<LitemallNoticeAdmin> {

}
