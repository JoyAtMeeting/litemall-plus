package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallTenant;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 租户表，逻辑分库 服务类
 * </p>
 *
 * @author ysling
 */
public interface ITenantService extends IBaseService<LitemallTenant> {

}
