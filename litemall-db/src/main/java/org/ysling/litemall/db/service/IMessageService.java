package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallMessage;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * webSocket消息 服务类
 * </p>
 *
 * @author ysling
 */
public interface IMessageService extends IBaseService<LitemallMessage> {

}
