package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallBrand;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 品牌商表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IBrandService extends IBaseService<LitemallBrand> {

}
