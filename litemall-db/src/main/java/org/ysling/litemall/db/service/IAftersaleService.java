package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallAftersale;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 售后表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IAftersaleService extends IBaseService<LitemallAftersale> {

}
