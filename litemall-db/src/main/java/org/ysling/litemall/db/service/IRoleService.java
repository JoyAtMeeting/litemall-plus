package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallRole;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IRoleService extends IBaseService<LitemallRole> {

}
