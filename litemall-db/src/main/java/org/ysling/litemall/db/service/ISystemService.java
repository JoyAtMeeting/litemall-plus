package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallSystem;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 系统配置表 服务类
 * </p>
 *
 * @author ysling
 */
public interface ISystemService extends IBaseService<LitemallSystem> {

}
