package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallReward;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 赏金任务参与表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IRewardService extends IBaseService<LitemallReward> {

}
