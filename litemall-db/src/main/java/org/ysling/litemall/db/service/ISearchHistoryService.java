package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallSearchHistory;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 搜索历史表 服务类
 * </p>
 *
 * @author ysling
 */
public interface ISearchHistoryService extends IBaseService<LitemallSearchHistory> {

}
