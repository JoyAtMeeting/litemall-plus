package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallGroupon;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 团购活动表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IGrouponService extends IBaseService<LitemallGroupon> {

}
