package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallRegion;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 行政区域表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IRegionService extends IBaseService<LitemallRegion> {

}
