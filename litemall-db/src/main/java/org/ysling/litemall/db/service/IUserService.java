package org.ysling.litemall.db.service;

import org.ysling.litemall.db.domain.LitemallUser;
import org.ysling.litemall.db.mybatis.IBaseService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author ysling
 */
public interface IUserService extends IBaseService<LitemallUser> {

}
