package org.ysling.litemall.admin.model.issue.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Ysling
 */
@Data
public class IssueListResult implements Serializable {

}
